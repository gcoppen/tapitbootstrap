<?php if ($page['footer']): ?>
  <footer class="row-fluid">
    <div class="span12" id="footer">
      <?php print render($page['footer']); ?>
    </div>
  </footer>
<?php endif; ?>